#
# Copyright (c) 2016-2017 Catalyst.net Ltd
#
# This file is part of vagrant-puppetfile.
#
# vagrant-puppetfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# vagrant-puppetfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-puppetfile. If not, see
# <http://www.gnu.org/licenses/>.
#

$: << File.expand_path('../lib', __FILE__)

require 'vagrant-puppetfile/version'

Gem::Specification.new do |s|
  s.name          = Vagrant::Puppetfile::NAME
  s.version       = Vagrant::Puppetfile::VERSION
  s.license       = 'GPL-3.0'
  s.author        = 'Evan Hanson'
  s.email         = 'evanh@catalyst.net.nz'
  s.summary       = 'Puppetfile provisioner'
  s.description   = 'Automatically installs puppet modules from a Puppetfile during Vagrant provisioning'
  s.homepage      = 'https://gitlab.com/catalyst-it/vagrant-puppetfile'
  s.files         = Dir['lib/**/*.rb']
end
