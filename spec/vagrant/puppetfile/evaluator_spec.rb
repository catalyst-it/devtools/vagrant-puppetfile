#
# Copyright (c) 2017 Catalyst.net Ltd
#
# This file is part of vagrant-puppetfile.
#
# vagrant-puppetfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# vagrant-puppetfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-puppetfile. If not, see
# <http://www.gnu.org/licenses/>.
#

describe Evaluator do
  let(:logger) { BlankObject.new }

  let(:evaluators) { Array.new }

  before do
    available = Set[*evaluators] & described_class::EVALUATORS
    missing   = Set[*evaluators] ^ described_class::EVALUATORS

    available.each do |e|
      allow(Open3).to receive(:capture2e).with(e, any_args).and_return([Object.new, SuccessObject.new])
    end

    missing.each do |e|
      allow(Open3).to receive(:capture2e).with(e, any_args).and_raise(SystemCallError, String.new)
    end
  end

  context 'with no evaluators available' do
    it 'should autodetect no evaluators' do
      expect(described_class.autodetect(logger)).to be_falsey
    end

    it 'should fail to create any kind of instance' do
      described_class::EVALUATORS.each do |e|
        expect { described_class.create(e, logger) }.to raise_error described_class::EvaluatorError
      end
    end

    it 'should fail to create any kind of instance automatically' do
      expect { described_class.create('autodetect', logger) }.to raise_error described_class::ConfigurationError
    end
  end

  context 'with the puppet evaluator available' do
    let(:evaluators) { ['puppet'] }

    it 'should autodetect an evaluator' do
      expect(described_class.autodetect(logger)).to be_truthy
    end

    it 'should create a puppet evaluator' do
      expect(described_class.create('puppet', logger)).to be_a described_class::Puppet
    end

    it 'should create a puppet evaluator automatically' do
      expect(described_class.create('autodetect', logger)).to be_a described_class::Puppet
    end
  end

  context 'with the librarian-puppet and puppet evaluators available' do
    let(:evaluators) { ['librarian-puppet', 'puppet'] }

    it 'should autodetect an evaluator' do
      expect(described_class.autodetect(logger)).to be_truthy
    end

    it 'should create either kind of evaluator' do
      ['librarian-puppet', 'puppet'].each do |e|
        expect(described_class.create(e, logger)).to be_a described_class::Base
      end
    end

    it 'should create a librarian-puppet evaluator automatically' do
      expect(described_class.create('autodetect', logger)).to be_a described_class::Librarian
    end
  end
end
