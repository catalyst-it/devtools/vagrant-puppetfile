#
# Copyright (c) 2017 Catalyst.net Ltd
#
# This file is part of vagrant-puppetfile.
#
# vagrant-puppetfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# vagrant-puppetfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-puppetfile. If not, see
# <http://www.gnu.org/licenses/>.
#

require 'logger'
require 'vagrant'
require 'vagrant-puppetfile'

class BlankObject < BasicObject
  def method_missing(*_); end
end

class FakeLogger < Logger
  alias detail info

  def initialize
    @i, @o = IO.pipe
    super(@o)
  end

  def output
    @output ||= begin
      @o.close
      @i.read.tap do
        @i.close
      end
    end
  end
end

class SuccessObject < BasicObject
  def success?
    true
  end
end

include Vagrant::Puppetfile

EVALUATORS = Evaluator::EVALUATORS.reject do |type|
  Evaluator.create(type, BlankObject.new) rescue false
end

RSpec.configure do |config|
  config.formatter = 'documentation'
  config.filter_run_excluding evaluators: EVALUATORS
end
